# docopt-turtle-cloudhaskell-template

This is a starting template for a [CloudHaskell](http://haskell-distributed.github.io/) 
application. It combines with the [turtle](https://hackage.haskell.org/package/turtle)
Shell monad to create a basic distributed shell engine that can
communicate via typed channels.

This repository automatically compiles with gitlab's ci to
produce an output binary.

Try it out like so:

```
stack build
stack exec -- myprog worker localhost 8081 &
stack exec -- myprog worker localhost 8082 &
stack exec -- myprog worker localhost 8083 &
stack exec -- myprog worker localhost 8084 &
```

And then

```
stack exec -- myprog master localhost 8080
```
