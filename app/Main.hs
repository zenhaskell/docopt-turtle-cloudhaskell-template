{-# LANGUAGE DeriveAnyClass     #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE QuasiQuotes        #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TemplateHaskell    #-}

module Main where

import           Control.Distributed.Process
                                         hiding ( catch )
import           Control.Distributed.Process.Node
                                                ( initRemoteTable
                                                , runProcess
                                                )
import           Control.Distributed.Process.Backend.SimpleLocalnet
import           Control.Distributed.Process.Closure
                                                ( remotable
                                                , mkClosure
                                                )
import qualified Control.Foldl                 as Fold
import           Control.Monad
import           Control.Monad.Catch
import           Data.Binary
import           Data.Char                      ( toUpper )
import           Data.Maybe
import           Data.Time
import           Data.Typeable
import           GHC.Generics
import           System.Console.Docopt
import           System.Environment             ( getArgs )
import           Turtle

patterns :: Docopt
patterns = [docoptFile|USAGE.txt|]

getArgOrExit = getArgOrExitWith patterns

newtype Payload = Payload { fileContent :: String }
  deriving (Binary, Generic, Show, Typeable)

data Response = Response { fileName :: String, timestamp :: String }
  deriving (Binary, Generic, Show, Typeable)

workerThread :: (SendPort Response, Payload) -> Process ()
workerThread (sPong, Payload x) = do
  wId <- processNodeId <$> getSelfPid
  let file =
        fromString
          $ map (\x -> if x `elem` [':', '/'] then '_' else x)
          . show
          $ wId
  liftIO $ writeTextFile file (fromString x)
  response <-
    liftIO $ flip fold Fold.head $ Response (show file) . show <$> datefile file
  sendChan sPong (fromJust response)

$(remotable ['workerThread])

masterThread :: [NodeId] -> Process ()
masterThread workers = do
  (sPong, rPong) <- newChan :: Process (SendPort Response, ReceivePort Response)

  workers'       <-
    fmap (filter isJust) . forM workers $ \w -> flip catch (catcher w) $ do
      void $ spawn w ($(mkClosure 'workerThread) (sPong, Payload "Hello Node!"))
      return $ Just w

  replicateM_ (length workers') $ do
    x <- receiveChan rPong :: Process Response
    say $ show x
 where
  catcher :: NodeId -> IOError -> Process (Maybe a)
  catcher w e = do
    say $ "ERROR Could not send message to " ++ show w ++ ": " ++ show e
    return Nothing

remoteTable :: RemoteTable
remoteTable = __remoteTable initRemoteTable

nodePlugin :: String -> (Backend -> IO ()) -> (Arguments -> IO ())
nodePlugin role action args = when (args `isPresent` command role) $ do
  host    <- args `getArgOrExit` argument "host"
  port    <- args `getArgOrExit` argument "port"
  backend <- initializeBackend host port remoteTable
  action backend
  return ()

main = do
  args <- parseArgsOrExit patterns =<< getArgs

  nodePlugin "master" (`startMaster` masterThread) args
  nodePlugin "worker" startSlave                   args
